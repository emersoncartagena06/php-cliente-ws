<!DOCTYPE html>
<html lang="es-sv">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" 
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <title>Comparar números</title>
</head>
<body>
    <div class="container m-4">        
        <div class="card">
            <div class="card-header">
                <h4>Libros</h4>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Libro</th>
                        <th>Existencia</th>
                        <th>Precio</th>
                        <th>Autor</th>
                        <th>Editorial</th>
                        <th>Genero</th>
                        <th>Descripción</th>
                    </tr>
                </thead>
                <tbody>
                    <?

                        $url ="https://localhost:44360/LibroService.asmx?WSDL";
                        $context = stream_context_create(array(
                            'ssl' => array(
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                                'allow_self_signed' => true
                            )
                        ));
                            
                        $service = new SoapClient($url, array('stream_context' => $context));
                        $response = $service->ListarLibros();

                        foreach($response->ListarLibrosResult->Libro as $item){
                            echo "<tr><td>" . $item->codigoLibro . "</td><td>" . $item->nombreLibro . 
                            "</td><td>" . $item->existencia . "</td><td class=\"text-end\"> $" . $item->precio . 
                            "</td><td>" . $item->autor->nombre . "</td><td>" . $item->editorial->nombre . 
                            "</td><td>" . $item->genero->nombre . "</td><td>" . $item->descripcion . "</td></tr>";
                        }

                    ?>
                </tbody>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>